from abc import ABC, abstractmethod

class Animal(ABC):
    @abstractmethod
    def eat(self, food):
        pass

    @abstractmethod
    def make_sound(self):
        pass

    def call(self):
        pass

class Dog(Animal):
    def __init__(self, name, breed, age):
        self._name = name
        self._breed = breed
        self._age = age

    def get_name(self):
        return self._name

    def set_name(self, new_name):
        self._name = new_name

    def get_breed(self):
        return self._breed

    def set_breed(self, new_breed):
        self._breed = new_breed

    def get_age(self):
        return self._age

    def set_age(self, new_age):
        self._age = new_age
    
    def eat(self, food):
        print(f"Eaten {food}")

    def make_sound(self):
        print("Bark! Woof! Arf!")

    def call(self):
        print(f"Here {self._name}")

class Cat(Animal):
    def __init__(self, name, breed, age):
        self._name = name
        self._breed = breed
        self._age = age

    def get_name(self):
        return self._name

    def set_name(self, new_name):
        self._name = new_name

    def get_breed(self):
        return self._breed

    def set_breed(self, new_breed):
        self._breed = new_breed

    def get_age(self):
        return self._age

    def set_age(self, new_age):
        self._age = new_age
    
    def eat(self, food):
        print(f"Serve me {food}")

    def make_sound(self):
        print(f"Miao!, Nyaw, Nyaaaaa!")

    def call(self):
        print(f"{self._name}, come on!")


dog = Dog("Kulas", "Askal", 2)
cat = Cat("Mingming", "Pusang Gala", 3)


dog.eat("Steak")
dog.make_sound()
dog.call()

cat.eat("Tuna")
cat.make_sound()
cat.call()
